import os.path
import sys

# Чтение файла
from copy import deepcopy


def read_file(filename):
    # Открытие файла
    if not os.path.exists('program.txt'):
        raise Exception('Не удалось открыть файл', filename)
        sys.exit(-1)
    program_text = str()

    # Создание cписка символов
    with open(filename) as f:
        program_text = ' '.join(f.readlines())
        program_text = list(program_text)
    # print(program_text)
    return program_text


# Бинарный поиск по словам
def binary_find(lst, word):
    l, r = 0, len(lst)
    while l <= r:
        mid = (l + r) // 2
        if lst[mid] == word:
            return True
        elif lst[mid] < word:
            l = mid + 1
        else:
            r = mid - 1
    return False


def check_key(lst, pos, key, error_key=-10000):
    if list(lst[pos].keys())[0] != key:
        raise Exception('Not found key \'%s\'' % key)
        raise Exception(error_key)


def is_key(lst, pos, key):
    try:
        return list(lst[pos].keys())[0] == key
    except:
        return False


def in_keys(lst, pos, keys):
    return list(lst[pos].keys())[0] in keys


def is_id(lst, pos, value=None):
    is_id = list(lst[pos].keys())[0] == 'id'
    has_value = list(lst[pos].values())[0] == value
    return is_id if not value else is_id & has_value


def get_value(lst, pos):
    return list(lst[pos].values())[0]


def print_key_and_value(lst, pos):
    print('{\'%s\': %s}' % (list(lst[pos].keys())[0], list(lst[pos].values())[0]))


def check_variable(lst, pos, vars):
    value = get_value(lst, pos)
    variable = None
    for obj in vars:
        if obj.name == value:
            variable = obj
    if not variable:
        raise Exception('Unexpected variable \'%s\'' % value)
        raise Exception(-40000)
    pos += 1
    if is_key(lst, pos, '['):
        check_key(lst, pos, '[')
        pos += 1
        check_key(lst, pos, 'num')
        ident = get_value(lst, pos)
        pos += 1
        check_key(lst, pos, ']')
        if not isinstance(variable, ArrayVariable):
            raise Exception('Unexpected index \'[%s]\'' % ident)
        if variable.size <= ident:
            raise Exception('List out of range')
            raise Exception(-50000)
        pos += 1

    return variable, pos + 1


def is_float(st):
    try:
        float(st)
        return True
    except:
        return False


def is_int(st):
    try:
        int(st)
        return True
    except:
        return False


def is_bool(st):
    try:
        bool(st)
        return True
    except:
        return False


def check_type_const(lst, pos, type):
    value = get_value(lst, pos)
    if is_int(value):
        value_type = 'integer'
    elif is_float(value):
        value_type = 'float'
    elif is_bool(value):
        value_type = 'bool'
    else:
        raise Exception('Unexpected type of variable \'%s\'' % value)
        raise Exception(-60000)
    if value_type != type:
        raise Exception('Unexpected type of variable \'%s\'' % value)
        raise Exception(-60000)


def check_type_var(lst, pos, type, vars):
    var, _ = check_variable(lst, pos, vars)
    if var.type != type:
        raise Exception('Unexpected type of variable \'%s\'' % var.name)
        raise Exception(-60000)


def check_syntax(lst, pos, var, vars):
    type = var.type
    tokens = []
    if type == 'integer' or type == 'float':
        opers = ['+', '*']
    else:
        opers = ['rel', ]
    iterable = deepcopy(pos)
    while not is_key(lst, iterable, ';'):
        if is_key(lst, iterable, 'num'):
            check_type_const(lst, iterable, type)
        elif is_key(lst, iterable, 'id'):
            check_variable(lst, iterable, vars)
            if type != 'bool':
                check_type_var(lst, iterable, type, vars)
        elif not in_keys(lst, iterable, opers):
            raise Exception('Unexpected operation/token')
            raise Exception(-70000)
        tokens.append(lst[iterable])
        iterable += 1
    return tokens, iterable


def get_var_or_num_value(lst, pos, vars):
    value = None
    if is_key(lst, pos, 'num'):
        value = get_value(lst, pos)
    elif is_key(lst, pos, 'id'):
        value = check_variable(lst, pos, vars)[0].value
    elif is_key(lst, pos, SimpleVariable):
        value = get_value(lst, pos).value
    else:
        raise Exception(-80000)
    if value is None:
        raise Exception(-85000)
    if is_int(value):
        value = int(value)
    elif is_float(value):
        value = float(value)
    elif is_bool(value):
        value = bool(value)
    return value


def check_values(lst):
    for obj in lst:
        if obj is None:
            raise Exception('Variable is None')
            raise Exception(-90000)


def check_count_fig(lst):
    cnt = 0
    for pos, obj in enumerate(lst):
        if is_key(lst, pos, '}'):
            cnt += 1
        if is_key(lst, pos, '{'):
            cnt -= 1
    if cnt:
        raise Exception('Count \'{\' does not match \'}\'')
        raise Exception(-1000)


def check_for_syntax(lst, pos, var, vars):
    type = var.type
    if type != 'integer':
        raise Exception('Expected integer value')
        raise Exception(-2000)
    iterable = deepcopy(pos)
    while not is_key(lst, iterable, '{'):
        if is_key(lst, iterable, 'num'):
            check_type_const(lst, iterable, type)
        elif is_key(lst, iterable, 'id'):
            check_variable(lst, iterable, vars)
            check_type_var(lst, iterable, type, vars)
        iterable += 1


def check_for_desc(lst, pos, vars):
    first, _ = check_variable(lst, pos, vars)
    check_for_syntax(lst, pos, first, vars)
    pos += 1
    check_key(lst, pos, 'ass')
    pos += 1
    second = get_var_or_num_value(lst, pos, vars)
    pos += 1
    if is_key(lst, pos, 'to'):
        is_to = True
    elif is_key(lst, pos, 'downto'):
        is_to = False
    else:
        is_to = None
        raise Exception('Expected \'to|downto\'')
        raise Exception(-3000)
    pos += 1
    third = get_var_or_num_value(lst, pos, vars)
    pos += 1
    check_key(lst, pos, 'do')
    pos += 1
    check_key(lst, pos, '{')
    return first, second, third, is_to, pos + 1


def get_end_for(lst, pos):
    while not is_key(lst, pos, '}'):
        pos += 1
    return pos


def get_call_data(tokens, var, stack_call, count_calls):
    out_str = f"{var.name} <- "
    for out_pos, token in enumerate(tokens):
        if isinstance(list(token.values())[0], SimpleVariable) or isinstance(list(token.values())[0],
                                                                             ArrayVariable):
            out_str = out_str + f'{list(token.values())[0].name} '
        if list(token.keys())[0] == '*':
            if list(token.values())[0] == 1:
                oper = '* '
            elif list(token.values())[0] == 2:
                oper = '/ '
            out_str = out_str + oper
        if list(token.keys())[0] == '+':
            if list(token.values())[0] == 1:
                oper = '+ '
            elif list(token.values())[0] == 2:
                oper = '- '
            out_str = out_str + oper
        if list(token.keys())[0] == 'rel':
            if list(token.values())[0] == 1:
                oper = '< '
            elif list(token.values())[0] == 2:
                oper = '<= '
            elif list(token.values())[0] == 3:
                oper = '> '
            elif list(token.values())[0] == 4:
                oper = '>= '
            elif list(token.values())[0] == 5:
                oper = '== '
            elif list(token.values())[0] == 6:
                oper = '!= '
            out_str = out_str + oper
        if is_id(tokens, out_pos) or is_key(tokens, out_pos, 'num'):
            out_str = out_str + str(get_value(tokens, out_pos)) + ' '
    stack_call.append(f'{count_calls}:\t' + out_str + '\n')
    count_calls = count_calls + 1
    return count_calls, stack_call


def check_opers(out_buffer, iterable, lst_segm_data, count_temp_vars, count_calls, stack_call):
    var, iterable = check_variable(out_buffer, iterable, lst_segm_data)
    tokens, new_iterable = check_syntax(out_buffer, iterable, var, lst_segm_data)
    count_calls, stack_call = get_call_data(tokens, var, stack_call, count_calls)
    # print(tokens)
    num = 0
    while num < len(tokens):
        token = tokens[num]
        if list(token.keys())[0] == '*':
            val1 = get_var_or_num_value(tokens, num - 1, lst_segm_data)
            val2 = get_var_or_num_value(tokens, num + 1, lst_segm_data)
            check_values([val1, val2])
            result = 0
            if list(token.values())[0] == 1:
                result = val1 * val2
            elif list(token.values())[0] == 2:
                result = val1 // val2
            else:
                raise Exception('Unexpected operation')
                raise Exception(-100000)
            new_var = SimpleVariable('$%s' % count_temp_vars, result)
            lst_segm_data.append(new_var)
            count_temp_vars += 1
            tokens.pop(num + 1)
            tokens.pop(num)
            tokens.pop(num - 1)
            tokens.insert(num - 1, {SimpleVariable: new_var})
            num = -1
            count_calls, stack_call = get_call_data(tokens, var, stack_call, count_calls)
        num += 1

    num = 0
    while num < len(tokens):
        token = tokens[num]
        if list(token.keys())[0] == '+':
            val1 = get_var_or_num_value(tokens, num - 1, lst_segm_data)
            val2 = get_var_or_num_value(tokens, num + 1, lst_segm_data)
            check_values([val1, val2])
            result = 0
            if list(token.values())[0] == 1:
                result = val1 + val2
            elif list(token.values())[0] == 2:
                result = val1 - val2
            else:
                raise Exception('Unexpected operation')
                raise Exception(-100000)
            new_var = SimpleVariable('$%s' % count_temp_vars, result)
            lst_segm_data.append(new_var)
            count_temp_vars += 1
            tokens.pop(num + 1)
            tokens.pop(num)
            tokens.pop(num - 1)
            tokens.insert(num - 1, {SimpleVariable: new_var})
            num = -1
            # print(tokens)
            count_calls, stack_call = get_call_data(tokens, var, stack_call, count_calls)
        num += 1

    num = 0
    while num < len(tokens):
        token = tokens[num]
        if list(token.keys())[0] == 'rel':
            val1 = get_var_or_num_value(tokens, num - 1, lst_segm_data)
            val2 = get_var_or_num_value(tokens, num + 1, lst_segm_data)
            check_values([val1, val2])
            result = 0
            if list(token.values())[0] == 1:
                result = val1 < val2
            elif list(token.values())[0] == 2:
                result = val1 <= val2
            elif list(token.values())[0] == 3:
                result = val1 > val2
            elif list(token.values())[0] == 4:
                result = val1 >= val2
            elif list(token.values())[0] == 5:
                result = val1 == val2
            elif list(token.values())[0] == 6:
                result = val1 != val2
            else:
                raise Exception('Unexpected operation')
                raise Exception(-100000)
            new_var = SimpleVariable('$%s' % count_temp_vars, result)
            lst_segm_data.append(new_var)
            count_temp_vars += 1
            tokens.pop(num + 1)
            tokens.pop(num)
            tokens.pop(num - 1)
            tokens.insert(num - 1, {SimpleVariable: new_var})
            num = -1
            count_calls, stack_call = get_call_data(tokens, var, stack_call, count_calls)
            # print(tokens)
        num += 1

    var.value = get_var_or_num_value(tokens, 0, lst_segm_data)
    iterable = new_iterable
    return iterable, lst_segm_data, count_temp_vars, count_calls, stack_call


class SimpleVariable:
    def __init__(self, name, value=None, type=None):
        self.name = name
        self.value = value
        self.type = type


class ArrayVariable:
    def __init__(self, name, size, type=None):
        self.name = name
        self.values = dict()
        self.size = size
        self.type = type

    def __getitem__(self, key):
        return self.values[key]

    def __setitem__(self, key, value):
        self.values[key] = value
