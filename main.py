import PySimpleGUI as sg

from main_func import main, get_out_stack_var
from utils import read_file

if __name__ == '__main__':
    layout = [
        [
            sg.Text('File input:'),
            sg.InputText(),
            sg.FileBrowse(change_submits=True),
            sg.Button('Submit'),
        ],
        [
            sg.Output(size=[50, 30], key="file_input"),
            sg.Output(size=[50, 30], key="stack_callable"),
            sg.Output(size=[25, 30], key="stack_variable"),
        ],
        [
            sg.Output(size=[134, 5])
        ],
        [sg.Submit(), sg.Cancel()]
    ]
    window = sg.Window('Fedorin D. V.', layout)
    program_text = list()
    while True:  # The Event Loop
        event, values = window.read()
        if event == 'Submit':
            filename = values['Browse']
            program_text = read_file(filename)
            window['file_input'].update(''.join(program_text))
        if event == 'Submit0':
            try:
                lst_segm_data, stack_call = main(program_text)
                window['stack_variable'].update(get_out_stack_var(lst_segm_data))
                window['stack_callable'].update(''.join(stack_call))
            except Exception as e:
                print(e)
        # print(event, values) #debug
        if event in (None, 'Exit', 'Cancel'):
            break
