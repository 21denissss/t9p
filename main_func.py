from utils import *

key_words = ['array',
             'and',
             'or',
             'do',
             'endprogr',
             'data',
             'var',
             'is',
             'code',
             'for',
             'to',
             'downto',
             'not',
             'true',
             'false',
             'progr',
             'of'
             ]

simple_types = ['integer', 'float', 'bool']


def main(program_text):
    # print(program_text)
    key_words.sort()
    sost = 0
    pos = 0
    identifier = ''
    digit = ''
    out_buffer = list()
    while pos < len(program_text):
        if sost == 0:
            if program_text[pos] == '<':
                sost = 1
            elif program_text[pos] == '>':
                sost = 2
            elif program_text[pos] == '=':
                sost = 11
            elif program_text[pos] == '!':
                sost = 12
            elif program_text[pos] == ';':
                out_buffer.append({';': 0})
            elif program_text[pos] == ',':
                out_buffer.append({',': 0})
            elif program_text[pos] == '.':
                out_buffer.append({'.': 0})
            elif program_text[pos] == '[':
                out_buffer.append({'[': 0})
            elif program_text[pos] == ']':
                out_buffer.append({']': 0})
            elif program_text[pos] == '{':
                out_buffer.append({'{': 0})
            elif program_text[pos] == '}':
                out_buffer.append({'}': 0})
            elif program_text[pos] == '*':
                out_buffer.append({'*': 1})
            elif program_text[pos] == '/':
                sost = 3
            elif program_text[pos] == '+':
                out_buffer.append({'+': 1})
            elif program_text[pos] == '-':
                out_buffer.append({'+': 2})
            elif program_text[pos].isalpha():
                identifier += program_text[pos]
                sost = 6
            elif program_text[pos].isdigit():
                digit += program_text[pos]
                sost = 10
        elif sost == 1:
            if program_text[pos] == '=':
                out_buffer.append({'rel': 2})
            elif program_text[pos] == '-':
                out_buffer.append({'ass': 0})
            else:
                out_buffer.append({'rel': 1})
                pos -= 1
            sost = 0
        elif sost == 2:
            if program_text[pos] == '=':
                out_buffer.append({'rel': 4})
            else:
                out_buffer.append({'rel': 3})
                pos -= 1
            sost = 0
        elif sost == 3:
            if program_text[pos] == '*':
                sost = 4
            else:
                out_buffer.append({'*': 2})
                sost = 0
        elif sost == 4:
            if program_text[pos] == '*':
                sost = 5
        elif sost == 5:
            if program_text[pos] == '/':
                out_buffer.append({'com': 0})
                sost = 0
            else:
                sost = 4
        elif sost == 6:
            if program_text[pos].isalpha() or program_text[pos].isdigit():
                identifier += program_text[pos]
            else:
                if binary_find(key_words, identifier):
                    out_buffer.append({identifier: 0})
                else:
                    out_buffer.append({'id': identifier})
                pos -= 1
                identifier = ''
                sost = 0
        elif sost == 10:
            if program_text[pos].isdigit():
                digit += program_text[pos]
            elif program_text[pos] == '.':
                digit += program_text[pos]
                sost = 13
            elif program_text[pos] == 'e':
                digit += program_text[pos]
                sost = 15
            else:
                out_buffer.append({'num': digit})
                pos -= 1
                digit = ''
                sost = 0
        elif sost == 11:
            if program_text[pos] == '=':
                out_buffer.append({'rel': 5})
            else:
                print('Ошибка в автомате операций сравнения!')
                raise Exception(-1)
            sost = 0
        elif sost == 12:
            if program_text[pos] == '=':
                out_buffer.append({'rel': 6})
            else:
                print('Ошибка в автомате операций сравнения!')
                raise Exception(-1)
            sost = 0
        elif sost == 13:
            if program_text[pos].isdigit():
                digit += program_text[pos]
                sost = 14
            else:
                print('Ошибка в автомате чисел! (Состояние 13)')
                raise Exception(-1)
        elif sost == 14:
            if program_text[pos].isdigit():
                digit += program_text[pos]
            elif program_text[pos] == 'e':
                digit += program_text[pos]
                sost = 15
            else:
                out_buffer.append({'num': digit})
                pos -= 1
                digit = ''
                sost = 0
        elif sost == 15:
            if program_text[pos].isdigit():
                digit += program_text[pos]
                sost = 17
            elif program_text[pos] == '+' or program_text[pos] == '-':
                digit += program_text[pos]
                sost = 16
            else:
                print('Ошибка в автомате чисел! (Состояние 15)')
                raise Exception(-1)
        elif sost == 16:
            if program_text[pos].isdigit():
                digit += program_text[pos]
                sost = 17
            else:
                print('Ошибка в автомате чисел! (Состояние 16)')
                raise Exception(-1)
        elif sost == 17:
            if program_text[pos].isdigit():
                digit += program_text[pos]
            else:
                out_buffer.append({'num': digit})
                pos -= 1
                digit = ''
                sost = 0
        else:
            pass
        pos += 1

    out_buffer1 = []

    for obj in out_buffer:
        for k, v in obj.items():
            if k != 'com':
                out_buffer1.append({k: v})

    out_buffer = out_buffer1

    # iter = 0
    # for obj in out_buffer:
    #     for k, v in obj.items():
    #         print('%s: {\'%s\': %s}' % (iter, k, v))
    #         iter += 1
    # print('------------------')

    # Program
    check_key(out_buffer, 0, 'progr')
    if not is_id(out_buffer, 1):
        print('Not found id program')
        raise Exception()

    check_count_fig(out_buffer)

    # Data
    lst_segm_data = []
    check_key(out_buffer, 2, 'data')
    check_key(out_buffer, 3, '{')
    iterable = 4
    while not is_key(out_buffer, iterable, '}'):
        check_key(out_buffer, iterable, 'var')
        while not is_key(out_buffer, iterable, 'is'):
            iterable += 1
            lst_segm_data.append(SimpleVariable(get_value(out_buffer, iterable)))
            iterable += 1
            if is_key(out_buffer, iterable, ','):
                pass
        iterable += 1
        if not get_value(out_buffer, iterable) in simple_types:
            if not is_key(out_buffer, iterable, 'array'):
                print('Not found type \'%s\'' % get_value(out_buffer, iterable))
                raise Exception(-30000)
            iterable += 1
            check_key(out_buffer, iterable, '[')
            iterable += 1
            check_key(out_buffer, iterable, 'num')
            size = get_value(out_buffer, iterable)
            cnt_arrays = 0
            array_variables = []
            for var in lst_segm_data:
                if not var.type:
                    cnt_arrays += 1
            for i in range(cnt_arrays):
                array_variables.append(ArrayVariable(lst_segm_data[-cnt_arrays + i].name, size))
            lst_segm_data = lst_segm_data[:-cnt_arrays]
            iterable += 1
            check_key(out_buffer, iterable, ']')
            iterable += 1
            check_key(out_buffer, iterable, 'of')
            iterable += 1
            if not get_value(out_buffer, iterable) in simple_types:
                print('Not found type \'%s\'' % get_value(out_buffer, iterable))
                raise Exception(-30000)
            for var in array_variables:
                var.type = get_value(out_buffer, iterable)
            lst_segm_data.extend(array_variables)
        for var in lst_segm_data:
            if isinstance(var, SimpleVariable) and not var.type:
                var.type = get_value(out_buffer, iterable)
        iterable += 1
        check_key(out_buffer, iterable, ';')
        iterable += 1
    check_key(out_buffer, iterable, '}')

    # Code
    iterable += 1
    check_key(out_buffer, iterable, 'code')
    iterable += 1
    check_key(out_buffer, iterable, '{')
    iterable += 1
    count_temp_vars = 0
    count_calls, stack_call = 0, []
    while not is_key(out_buffer, iterable, 'endprogr'):
        if is_key(out_buffer, iterable, '}'):
            iterable += 1
            continue
        if not is_key(out_buffer, iterable, 'for'):
            iterable, lst_segm_data, count_temp_vars, count_calls, stack_call = check_opers(out_buffer, iterable,
                                                                                            lst_segm_data,
                                                                                            count_temp_vars,
                                                                                            count_calls, stack_call)
        else:
            check_key(out_buffer, iterable, 'for')

            pos_end_for = get_end_for(out_buffer, iterable)

            iterable += 1
            first, second, third, is_to, iterable = check_for_desc(out_buffer, iterable, lst_segm_data)

            for_pos = iterable
            if is_to:
                while second < third:
                    if is_key(out_buffer, iterable, '}'):
                        iterable = for_pos
                    iterable, lst_segm_data, count_temp_vars, count_calls, stack_call = check_opers(out_buffer,
                                                                                                    iterable,
                                                                                                    lst_segm_data,
                                                                                                    count_temp_vars,
                                                                                                    count_calls,
                                                                                                    stack_call)
                    iterable += 1
                    second += 1
            else:
                while second > third:
                    if is_key(out_buffer, iterable, '}'):
                        iterable = for_pos
                    iterable, lst_segm_data, count_temp_vars, count_calls, stack_call = check_opers(out_buffer,
                                                                                                    iterable,
                                                                                                    lst_segm_data,
                                                                                                    count_temp_vars,
                                                                                                    count_calls,
                                                                                                    stack_call)
                    iterable += 1
                    second -= 1
            iterable = pos_end_for
            first.value = third
        iterable += 1

    return lst_segm_data, stack_call


def get_out_stack_var(lst_segm_data):
    size = 8
    out_str = ""
    for pos, var in enumerate(lst_segm_data):
        if isinstance(var, SimpleVariable):
            real_pos = size * pos
            out_str += f"{real_pos}:\t{var.name}\t{var.value}\t8\n"
    return out_str
